$icon = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"><g><path d="M437.019,74.98C388.667,26.629,324.38,0,256,0C187.619,0,123.332,26.629,74.98,74.98C26.629,123.332,0,187.62,0,256 s26.629,132.667,74.98,181.019C123.332,485.371,187.62,512,256,512s132.667-26.629,181.019-74.98 C485.371,388.667,512,324.38,512,256S485.371,123.333,437.019,74.98z M378.306,195.073L235.241,338.139 c-2.929,2.929-6.768,4.393-10.606,4.393c-3.839,0-7.678-1.464-10.607-4.393l-80.334-80.333c-5.858-5.857-5.858-15.354,0-21.213 c5.857-5.858,15.355-5.858,21.213,0l69.728,69.727l132.458-132.46c5.857-5.858,15.355-5.858,21.213,0 C384.164,179.718,384.164,189.215,378.306,195.073z"/></g></svg>`
$night = false;
class Charts {
	constructor(element,obj,map=false) {
		var colX = obj.x.column.length;
		this.maxY = 0;
		this.objData = obj;
		this.nowOnScreen = [];
		for (var ch in obj) {
			this.nowOnScreen[ch]= [];
			if (obj[ch].type == "line") {
				var m = Math.max.apply(null, obj[ch].column);
				if (m >this.maxY) this.maxY = m;
			}
		}
		this.svgChart = element;
		this.dates = obj.x.column;
		this.map = map;
		this.begin = parseInt($(this.svgChart).css('height'))-30 ;
		this.width = parseInt($(this.svgChart).css('width'));
		this.colX = colX;
		this.leftProc =  Math.round(this.colX / (parseInt($("#map").css("width"))/parseInt($("#left_shadow").css("width"))));
		this.rightProc =  Math.round(this.colX / (parseInt($("#map").css("width"))/parseInt($("#right_shadow").css("width"))));
		this.procentOfSize = parseInt($("#map").css("width"))/parseInt($("#select_area").css("width")+14);
		if (map) {
			this.stepX = (this.width / colX)
		}else{
		   this.stepX = (this.width / colX) *this.procentOfSize; 
		}
		this.stepY =((parseInt($(this.svgChart).css('height'))-50) / this.maxY);
		this.points = [];
		this.nowOnScreen = [];
		for(var i in obj){
			if (i != "diff") this.nowOnScreen[i]=[];
		}
		// по умолчанию кординаты считаются от верхнего края, а нам надо от нижнего, поэтому берем за начало высоту блока svg
	}
	updateSizes(){
		this.oldStepX = this.stepX;
		this.oldStepY = this.stepY;
		this.procentOfSize = parseInt($("#map").css("width"))/parseInt($("#select_area").css("width")+14);
		this.stepX = (this.width / this.colX) *this.procentOfSize *0.96;
		this.proc = Math.round($chart.colX/$chart.procentOfSize); 
		this.leftProc = Math.round(this.colX / (parseInt($("#map").css("width"))/parseInt($("#left_shadow").css("width"))));
		this.rightProc =  Math.round(this.colX / (parseInt($("#map").css("width"))/parseInt($("#right_shadow").css("width"))));
		this.stepY = ((parseInt($(this.svgChart).css('height'))-70) / this.maxY);
	}
	// updateProcs(){
	// 	this.leftProc = Math.round(this.colX / (parseInt($("#map").css("width"))/parseInt($("#left_shadow").css("width"))));
	// 	this.rightProc =  Math.round(this.colX / (parseInt($("#map").css("width"))/parseInt($("#right_shadow").css("width"))));
	// }
	// отрисовка графика из массива координат
	drawChart(arr, name) {
		// на вход получаем массив такой же, как в chart_data , только без названия (например "y0")
		let chart = document.createElementNS('http://www.w3.org/2000/svg', 'g');
		chart.setAttributeNS(null, 'class', `${name}`);
		this.svgChart.append(chart);
		// создаем пары x,y для всех точек графика
		var points = `0,${this.begin - arr[0] * this.stepY} `;
		for (var i = 1; i < arr.length; i++) {
			// отрисовываем линии графика:
			// по иксу в нашей системе координат идут единичные отрезки, величиной в stepX
			// по игреку мы инвертируем график вычитая из this.begin
			// по игреку умножаем значение на величину единичного пункта в пикселях
			points += `${Math.round((i + 1) * this.stepX)},${Math.round(this.begin - arr[i] * this.stepY)} `;
		//     this.drawLine(i * this.stepX, this.begin - arr[i - 1] * this.stepY, (i + 1) * this.stepX, this.begin - arr[i] * this.stepY, chart);
		}
		// polyline - ломаная линия проходящая через все заданные точки
		let polyline = document.createElementNS('http://www.w3.org/2000/svg', 'polyline');
		polyline.setAttributeNS(null, 'points', points);
		polyline.setAttributeNS(null, 'fill', 'none');
		polyline.setAttributeNS(null, 'stroke-width', '4');
		if (!this.map) polyline.setAttributeNS(null, 'name', name);
		$(chart).append(polyline);
		// рисуем поверх полилайна точки
		for (var i = 0; i < arr.length; i++) {
			let circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
			circle.setAttributeNS(null, 'cx', `${(i + 1) * this.stepX}`);
			circle.setAttributeNS(null, 'cy', `${this.begin - arr[i] * this.stepY}`);
			circle.setAttributeNS(null, 'r', "10");
			circle.setAttributeNS(null, 'value', `${arr[i]}`);
			let data = new Date(this.dates[i]);
			
			if(!this.map){
				circle.setAttributeNS(null, 'date', `${data.toDateString()}`);

			}

			$(chart).append(circle);
		}

	}
	drawOnScreen(arr){
		$("circle").remove();
		var difference = [];
		var maxY = 0;
		for (var i in this.nowOnScreen) {
			if (i != "x") {
				let m = Math.max.apply(null, arr[i]);
				if (m > maxY) maxY = m;
			}
				this.nowOnScreen[i] = arr[i];
				if (i == "x") continue;
				let polyline = document.getElementsByName(i)[0];
				var points = `0,${this.begin - this.nowOnScreen[i][0] * this.stepY} `;
				for (var x = 1; x < this.nowOnScreen[i].length; x++) {
					points += `${Math.round(x * this.stepX)},${this.begin - (this.nowOnScreen[i][x] * this.stepY)} `;
				}
				polyline.setAttributeNS(null, 'points', points);
		}
		this.maxY = maxY;
		if (this.oldStepY != this.stepY) {
			this.systemOfCord();
		}
		
	}
	drawCircles(night=false){
		$("circle").remove();
		for (var i in this.nowOnScreen) {
			if (i == "x") continue;
			let polyline = document.getElementsByName(i)[0];
			for (var x = 0; x < this.nowOnScreen[i].length; x++) {
				let circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
				if (x==0){ circle.setAttributeNS(null, 'cx', `0`);}
				else {circle.setAttributeNS(null, 'cx', `${x* this.stepX}`);}
				circle.setAttributeNS(null, 'cy', `${this.begin - this.nowOnScreen[i][x] * this.stepY}`);
				circle.setAttributeNS(null, 'r', "10");
				circle.setAttributeNS(null, 'value', `${this.nowOnScreen[i][x]}`);
				circle.setAttributeNS(null, 'chart', i);

				if ($night) {
					$(circle).css({
						fill: 'rgb(36, 47, 62)',
						stroke: this.objData[i].color,
						'stroke-width': 4
					})
				}else{
					$(circle).css({
						fill: 'white',
						stroke: this.objData[i].color,
						'stroke-width': 4
					})
				}

				let data = new Date(this.nowOnScreen["x"][x]);
				if(!this.map) circle.setAttributeNS(null, 'date', `${data.toDateString()}`);
				$(polyline).parent().append(circle);
			}
		}
	}
	// отрисовка одной линии
	drawLine(cordx1, cordy1, cordx2, cordy2, group = '') {
		let path = document.createElementNS('http://www.w3.org/2000/svg', 'path');
		// на вход приходят float поэтому я их окруляю
		if (isNaN(cordy1))
			return false;

		path.setAttributeNS(null, 'd', `M${Math.round(cordx1)}, ${Math.round(cordy1)} L${Math.round(cordx2)}, ${Math.round(cordy2)}`);
		// на концах линии надо точки ставить иначе коряво получается
		let circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
		circle.setAttributeNS(null, 'cx', `${Math.round(cordx2)}`);
		circle.setAttributeNS(null, 'cy', `${Math.round(cordy2)}`);
		circle.setAttributeNS(null, 'r', "10");
		// добавляем в группу, если она есть(группируем линии в единый график)
		if (group != '') {
			$(group).append(path);
			$(group).append(circle);
		} else {
			this.svgChart.append(path);
			this.svgChart.append(circle);
		}
	}


	// функции для округления, нужны для постройки горизонтальных линий
	round100(val) {
		return Math.round(val / 100) * 100;
	}

	round50(val) {
		return Math.round(val / 50) * 50;
	}

	// рисует горизонтальные линии координат

	// строит ось дат (X)
	datesAxis(arr) { // arr - массив из дат
		// добавляем группу
		if (!document.getElementById("Axis")) {
			var d = document.createElementNS('http://www.w3.org/2000/svg', 'g');
			d.setAttributeNS(null, 'id', "Axis");
			this.svgChart.prepend(d);
		}else{
			var d = document.getElementById("Axis");
			$(d).empty();
		}
		
		// на оси надо отображать только 6 дат, поэтому находим эти 6 дат
		var step = arr.length / 6;
		var dates = [];
		for (var i = 0; i <= arr.length; i += step) {
			// здесь собираем 6 дат в массив в виде [номер даты в массиве(он пригодиться для верной расстановки на оси) , объект Date]
			dates.push([Math.round(i), new Date(arr[Math.round(i)])]);
		}

		// месяцы нужны для того чтобы отображать дату словами
		var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', "Oct", "Nov", "Dec"];
		// располагаем даты на оси
		for (var i = 0; i < dates.length-1; i++) {
			var text = document.createElementNS('http://www.w3.org/2000/svg', 'text');
			// умножаем длину единичного отрезка на номер даты в массиве
			text.setAttributeNS(null, 'x', Math.round(this.stepX * dates[i][0]));


			// даты должны быть под графиком, поэтому добавлем немного пикселей
			text.setAttributeNS(null, 'y', this.begin + 27);
			text.textContent = month[dates[i][1].getMonth()] + " " + dates[i][1].getDate();
			$(d).append(text);
		}
	}

	// строит систему координат
	systemOfCord() {
		if (!document.getElementById("cords")) {
			var cords = document.createElementNS('http://www.w3.org/2000/svg', 'g');
			cords.setAttributeNS(null, 'id', "cords");
			this.svgChart.prepend(cords);
		}else{
			var cordsOld = document.getElementById("cords");
			var cords = document.createElementNS('http://www.w3.org/2000/svg', 'g');
			cords.setAttributeNS(null, 'id', "cords");
			this.svgChart.prepend(cords);
			$(cords).append(`<animateTransform attributeName="transform" type="translate" from="0 ${this.begin}" to="0 0" begin="0s" dur="10s" repeatCount="1"></animateTransform>`)
			$(cordsOld).append(`<animateTransform attributeName="transform" type="translate" from="0 0" to="0 -${this.begin}" begin="0s" dur="10s" repeatCount="1"></animateTransform>`);
			$(cordsOld).remove()
		}
		//colX - количество дат в массиве,
		//this.maxY - максимальное количество
		var lines = [];
		if (this.maxY >= 250 && this.maxY < 500) {
			var round = this.round50(this.maxY),
				stepLines = round / 5;
		} else if (this.maxY >= 500) {
			var round = this.round100(this.maxY),
				stepLines = this.round50(round / 5);
		} else {
			//cверху хрень не нужная
			var stepLines = this.maxY / 5,
				round = this.maxY;
		}
		//засунули в массивчег
		for (var i = 0; i <= round; i += stepLines) {
			lines.push(Math.round(i));
		}


		for (var x = lines.length - 1; x >= 0; x--) {
			//stepY = begin_value_start/maxY
			this.drawHLine(0, Math.round(lines[x] * this.stepY), lines[x], cords);
		}
	}

	drawHLine(lines_x_coord_and_step_y, lines_x_coord_x, cont, group = '') {
		// максимальная длина линий
		var cordx2 = this.width;
		//рисуем линию
		let hLine = document.createElementNS('http://www.w3.org/2000/svg', 'path');
		//
		if (lines_x_coord_and_step_y == null) return false;
		hLine.setAttributeNS(null, 'd', `M ${lines_x_coord_and_step_y}, ${this.begin - lines_x_coord_x} H${cordx2}`);
		hLine.setAttributeNS(null, 'class', 'cords');
		// добавляем величину или текст для соответствующей линии
		var text = document.createElementNS('http://www.w3.org/2000/svg', 'text');
		text.setAttributeNS(null, 'x', lines_x_coord_and_step_y);
		text.setAttributeNS(null, 'y', this.begin - lines_x_coord_x - 3);
		//сам текст
		text.textContent = String(cont);
		let g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
		$(g).append(hLine)
		$(g).append(text)
		// опять же все это собираем в группу
		//Группа тега g для отрисовки линии и текста path, text
		if (group != '') {
			$(group).append(g);
		} else {
			this.svgChart.append(g);
		}

	}
}
function parseData(jsonObj) {
	var exportObj = [];
	for (var i = 0; i < jsonObj.length; i++) {
		exportObj[i] = [];
		for (var x = 0; x < jsonObj[i]["columns"].length; x++) {
			let name = jsonObj[i]["columns"][x][0],
				type = jsonObj[i]["types"][name],
				column = jsonObj[i]["columns"][x];
			column.splice(0, 1);
			if (type != "x") {
				exportObj[i][name] =
					{
						type: type,
						name: jsonObj[i]["names"][name],
						color: jsonObj[i]["colors"][name],
						column: column,
					}
			} else {
				exportObj[i][name] =
					{
						type: type,
						column: column,
					}
			}
		}
	}
	return exportObj;
}
function read(path) {
	var result = []
	$.ajax({
		type: "GET",
		async: false,
		url: path,
		success: function (res) {
			result = parseData(res);
		}
	})
	return result;
}
function makeChart(obj) {
	
	//Определили обьект в конструкторе передали значения
	$chart = new Charts($("#chart"),obj);
	$chart.updateSizes();
	
	let buildObj = [];
	
	for (var ch in obj) {
		if (obj[ch].type == "line") {
			let chart = document.createElementNS('http://www.w3.org/2000/svg', 'g');
			chart.setAttributeNS(null, 'class', `${ch}`);
			$chart.svgChart.append(chart);
			let polyline = document.createElementNS('http://www.w3.org/2000/svg', 'polyline');
			polyline.setAttributeNS(null, 'fill', 'none');
			polyline.setAttributeNS(null, 'stroke-width', '4');
			if (!$chart.map) polyline.setAttributeNS(null, 'name', ch);
			$(chart).append(polyline);
			buildObj[ch] = obj[ch].column.slice(obj[ch].column.length-2-Math.round($chart.proc),obj[ch].column.length-1);
			let popBlock = document.createElement("div");
			popBlock.setAttribute("style",`color: ${obj[ch].color};`);
			popBlock.setAttribute("class","popblock");
			popBlock.setAttribute("chart",ch);
			$("#values").append(popBlock);
			var check = $(`<div class="checkchart"><div class="icon"></div></div>`),
				icon = $($icon).attr("id",`icon_${ch}`).css({"fill":obj[ch].color,"stroke":obj[ch].color});
			check.children(".icon").append(icon);
			check.append(`<p class="checkp">${ch}</p>`);
			check.attr("chart",ch);
			check.attr("active","true");
			$("#checkcharts").append(check);
		}else{
			buildObj[ch] = obj[ch].column.slice(obj[ch].column.length-2-Math.round($chart.proc),obj[ch].column.length-1);
		}
	}
	$chart.drawOnScreen(buildObj);

	$chart.updateSizes();
	$chart.systemOfCord();
	$chart.datesAxis($chart.nowOnScreen.x);
	$chart.drawCircles();
	moveMap({data:1,clientX:1})
}
function makeMap(obj) {

	var map = new Charts($("#map"),obj,true);
	$chart.drawCircles();
	for (var ch in obj) {
		if (obj[ch].type == "line") {
			map.drawChart(obj[ch].column, ch);
			let elements = $("." + ch).children();
			elements.each(
				function (index, value) {
					if (value.tagName == 'circle') {
						$(this).css({
							fill: 'white',
							stroke: obj[ch].color,
							'stroke-width': 4
						})
					}
					else {
						$(this).css({
							stroke: obj[ch].color
						})
					}
				});
		}
	}
}
function moveMap(event) {
	let widthRight = parseInt($("#right_shadow").css("width")),
		widthLeft = parseInt($("#left_shadow").css("width"));
	if (event.data) {
		var difference = (event.data - event.clientX)/20;
	}else{
		var difference = ($beginX - event.changedTouches[0].screenX)/20;
	}
	widthLeft = widthLeft - difference;

	$("#right_shadow").css("width", `${ widthRight + difference}px`);
	$("#left_shadow").css("width", `${widthLeft}px`);
	$chart.updateSizes();
	let buildObj = [];
	for (var ch in $chart.objData) {
		buildObj[ch] = $chart.objData[ch].column.slice($chart.leftProc, $chart.objData[ch].column.length-$chart.rightProc);
	}
	$chart.drawOnScreen(buildObj);
	// $chart.updateSizes();

	// $chart.systemOfCord();
	$chart.datesAxis($chart.nowOnScreen.x);
	return false;
}
function chSizeLeft(e) {
	if (e.data) {
		var difference = (e.data - e.clientX)/20;
	}else{
		var difference = ($beginX - e.changedTouches[0].screenX)/20;
	}
	let	width = parseInt($("#select_area").css("width")),
		widthLeft = parseInt($("#left_shadow").css("width"));
	width+=difference;
	if (width + difference > 40) {
		$("#select_area").css("width",`${width}px`);
		$("#left_shadow").css("width",`${widthLeft - difference}px`);
	}
	$chart.updateSizes();
	let buildObj = [];
	for (var ch in $chart.objData) {
		buildObj[ch] = $chart.objData[ch].column.slice($chart.leftProc, $chart.objData[ch].column.length-$chart.rightProc);

	}

	$chart.drawOnScreen(buildObj);
	// $chart.systemOfCord();
	$chart.datesAxis($chart.nowOnScreen.x);
	return false;
}
function chSizeRight(e) {
	if (e.data) {
		var difference = (e.data - e.clientX)/20;
	}else{
		var difference = ($beginX - e.changedTouches[0].screenX)/20;
	}
	let	width = parseInt($("#select_area").css("width")),
		widthRight = parseInt($("#right_shadow").css("width"));
	width-=difference;
	if (width - difference > 40) {
		$("#select_area").css("width",`${width}px`);
		$("#right_shadow").css("width",`${widthRight + difference}px`);
	}
	$chart.updateSizes();
	let proc = $chart.colX/$chart.procentOfSize;
	let buildObj = [];
	for (var ch in $chart.objData) {
		buildObj[ch] = $chart.objData[ch].column.slice($chart.leftProc, $chart.objData[ch].column.length-$chart.rightProc);

	}
	$chart.drawOnScreen(buildObj);
	// $chart.systemOfCord();
	$chart.datesAxis($chart.nowOnScreen.x);
	return false;
}
function circleHover(e) {
	var newLine = document.createElementNS('http://www.w3.org/2000/svg','path');
	newLine.setAttribute('stroke-width','1');
	newLine.setAttribute('stroke','#e8e4e4');
	newLine.setAttribute('class','verticalpath');
	let x = $(e.currentTarget).offset().left;
	newLine.setAttribute('d',`M${x-18} 0 V400`);
	// $("#datepopup").css("left",`${x-25}px`);
	$(e.currentTarget).parent().parent().prepend(newLine);
	$("#datepopup").show();
	let sameDatearr = $("[date=\""+$(e.currentTarget).attr("date")+"\"]");
	for (var i = 0; i < sameDatearr.length; i++) {
		var chart = $(sameDatearr[i]).attr("chart"),
			value = $(sameDatearr[i]).attr("value");
		if (sameDatearr[i]!=e.currentTarget) {
			var sameDate = sameDatearr[i];
		}
		$(sameDatearr[i]).attr("style", $(sameDatearr[i]).attr("style")+";fill-opacity:1;stroke-opacity: 1");
		$(`.popblock[chart="${chart}"]`).empty();
		$(`.popblock[chart="${chart}"]`).append(`<p>${value}</p><p>${chart}</p>`);
	}
	$(".date").text($(e.currentTarget).attr("date"));

	// valPopUpP.text($(e.currentTarget).attr("value")+" ;"+$(sameDate).attr("value"));
}
function circleUnHover(event) {
	$("path.verticalpath").remove();
	let sameDatearr = $("[date=\""+$(event.currentTarget).attr("date")+"\"]");
	for (var i = 0; i < sameDatearr.length; i++) {
		if (sameDatearr[i]!=event.currentTarget) {
			var sameDate = sameDatearr[i];
		}
		$(sameDatearr[i]).attr("style", $(sameDatearr[i]).attr("style")+";fill-opacity:0;stroke-opacity: 0");
			
	}	   
	$("#datepopup").hide();
}
function chnigth(e) {
		$night = true;
		$("body").css({
			"background-color": "#242F3E",
			"color": "#FFF",
		});
		$("select").css({
			"background-color": "#323f50",
    		"border-color": "#323f50",
		});
		$("#datepopup").css({"border-color":"#212C3A","box-shadow": "0 1px 3px #212C3A"})
		$("#night>a ").text("To day mode");
		$("#night").attr("id","day");
		$chart.drawCircles(true);

		return false;
}
function chday(e) {
		$night = false;
		$("body").css({
			"background-color": "#FFF",
			"color": "#000",
		});
		$("select").css({
			"background-color": "#E8E4E4",
    		"border-color": "#E8E4E4",
    	});
    	$("#datepopup").css({"border-color":"#E6E6E6","box-shadow": "0 1px 3px #E6E6E6"})
		$("#day>a").text("To night mode");
		$("#day").attr("id","night");
		$chart.drawCircles();
		return false;
	}
$detachedMap = [];
$detachedpopblocks = [];
function checkon(e) {
	var chart = $(e.currentTarget).parent().attr("chart"),
		active =  $(e.currentTarget).parent().attr("active"),
		path = $(`#icon_${chart}`).children("g").children("path");
	if (active =="true") {
		$(e.currentTarget).parent().attr("active","false");
		$night ? path.css("fill","#242F3E") : path.css("fill","#FFF");
		path.css("stroke",$chart.objData[chart].color);
		delete $chart.nowOnScreen[chart];
		$detachedChart[chart] = $(".main ."+chart).detach();
		$detachedMap[chart] = $("#map ."+chart).detach();
		$detachedpopblocks[chart] = $(`.popblock[chart="${chart}"]`).detach();;
	}else{
		$(e.currentTarget).parent().attr("active","true");
		path.css("fill",$chart.objData[chart].color);
		$chart.nowOnScreen[chart] = [];
		$("#map").append($detachedMap[chart]);
		$("#chart").append($detachedChart[chart]);
		delete $detachedChart[chart];
		$("#values").append($detachedpopblocks[chart]);
	}
	let buildObj = [];
	for (var ch in $chart.objData) {
		buildObj[ch] = $chart.objData[ch].column.slice($chart.leftProc, $chart.objData[ch].column.length-$chart.rightProc);
	}
	$chart.drawOnScreen(buildObj);
	$chart.updateSizes();
	$chart.drawOnScreen(buildObj);
	$chart.drawCircles();
	if ($detachedChart.length < $chart.objData.length-1) {
		$chart.systemOfCord();
	}
}
$(document).ready(function () {
	var chartsObj = read("chart_data.json");
	$("body").css("height",window.screen.height+"px")
	function drCh(e) {
		$("svg").empty();
		$("#values").empty();
		$("#checkcharts").empty();
		var val = $('select').val();
		makeChart(chartsObj[val]);
		makeMap(chartsObj[val]);
		$detachedChart = [];

		// moveMap({data:1,clientX:1});
	}
	$("#nchart").change(drCh);
	
	let datePopUpP = $(".date");
	let valPopUpP = $(".valuePopUp");
	$("#left_shadow").css("width",parseInt($("#mask").css("width"))-214);
	drCh("e");
	moveMap({data:1,clientX:1});
	$("#chart").on("mouseenter","circle",circleHover);
	$("#chart").on("mouseleave","circle",circleUnHover);
	$("#chart").on("touchstart","circle",circleHover);
	$("#chart").on("touchend","circle",circleUnHover);
	var mask = $("#mask");

	$("#select_area").on("mousedown",function (e) {
		let beginX = e.clientX;
		mask.on("mousemove",beginX,moveMap)
		return false;
	})
	$("body").on("mouseup",function (e) {
		mask.off("mousemove",chSizeLeft);
		mask.off("mousemove",chSizeRight);
		mask.off("mousemove",moveMap);

		$chart.drawCircles();
		return false;
	})
	$("#leftChSize").on("mousedown",function (e) {
		let beginX = e.clientX;
		mask.on("mousemove",beginX,chSizeLeft);
		return false;
	})
	$("#rightChSize").on("mousedown",function (e) {
		let beginX = e.clientX;
		mask.on("mousemove",beginX,chSizeRight);
		return false;
	})
	document.getElementById("leftChSize").addEventListener("touchstart",function (e) {
		$beginX = e.changedTouches[0].screenX;
		document.getElementById("mask").addEventListener("touchmove",chSizeLeft);
		return false;
	})
	document.getElementById("rightChSize").addEventListener("touchstart",function (e) {
		$beginX = e.changedTouches[0].screenX;
		document.getElementById("mask").addEventListener("touchmove",chSizeRight);
		return false;
	})
	document.getElementById("select_area").addEventListener("touchstart",function (e) {
		$beginX = e.changedTouches[0].screenX;
		document.getElementById("mask").addEventListener("touchmove",moveMap);
		return false;
	})
	$("body").on("touchend",function (e) {
		document.getElementById("mask").removeEventListener("touchmove",chSizeLeft)
		document.getElementById("mask").removeEventListener("touchmove",chSizeRight)
		document.getElementById("mask").removeEventListener("touchmove",moveMap);
		$chart.drawCircles($night);
	})
	$night = false;
	$("body").on("click","#night", chnigth)
	$("body").on("click","#day",chday)
	$("body").on("touchend","#night", chnigth)
	$("body").on("touchend","#day", chday)
	$("body").on("click",".icon",checkon)

});